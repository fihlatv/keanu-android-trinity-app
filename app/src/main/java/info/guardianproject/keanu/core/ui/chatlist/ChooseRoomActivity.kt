package info.guardianproject.keanu.core.ui.chatlist

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import info.guardianproject.keanuapp.databinding.RecyclerViewBinding
import info.guardianproject.keanuapp.ui.BaseActivity

class ChooseRoomActivity: BaseActivity(), ChooseRoomAdapter.Listener {

    companion object {
        const val EXTRA_ROOM_ID = "room-id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = RecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.root.layoutManager = LinearLayoutManager(this)
        var cla = ChooseRoomAdapter(this);
        binding.root.adapter = cla
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = ""
    }

    override val activity: AppCompatActivity
        get() = this

    override fun updateListVisibility() {
        // Ignored.
    }

    override fun onSelected(roomId: String) {
        intent.putExtra(EXTRA_ROOM_ID, roomId)

        // Forward received intent. May contain additional data which needs further processing.
        setResult(RESULT_OK, intent)

        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}