package info.guardianproject.keanu.core.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.util.Debug
import info.guardianproject.keanuapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.file.FileService
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.model.message.getFileUrl
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.timeline.*
import org.matrix.android.sdk.internal.crypto.attachments.toElementToDecrypt
import timber.log.Timber
import java.util.*

class RemoteImService : Service() {


    private var mIsFirstTime = true

    private var mStatusBarNotifier: StatusBarNotifier? = null

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mRoomNotifyMap = HashMap<String?, Boolean>()

    private val mTimelines = HashMap<String, Timeline>()

    companion object {
        private const val PREV_CONNECTIONS_TRAIL_TAG = "prev_connections"
        private const val CONNECTIONS_TRAIL_TAG = "connections"
        private const val PREV_SERVICE_CREATE_TRAIL_TAG = "prev_service_create"
        private const val SERVICE_CREATE_TRAIL_KEY = "service_create"

        private val ALLOWED_ROOM_TYPES = listOf(
            EventType.MESSAGE,
            EventType.STICKER,
            EventType.REACTION,
            EventType.TYPING,
            EventType.RECEIPT,
            EventType.STATE_ROOM_NAME,
            EventType.STATE_ROOM_TOPIC,
            EventType.STATE_ROOM_MEMBER)

        /**
         * Standard filter for event lists. Will drop all events, which are
         *
         * - a redaction (The closest thing to a deletion, we can have in Matrix.)
         * - not of any type listed in `ALLOWED_ROOM_TYPES`.
         * - an edition of another event (Matrix SDK will show the edition on the original event, typically, anyway.)
         *
         * You would typically use this as predicate in a [Iterable.filter] call in a [Timeline.Listener]!
         *
         * @return true, if ok, false if it should be discarded.
         */
        val filterEvents = { te: TimelineEvent ->
            // This is ordered in perceived weight of executed code!
            !te.root.isRedacted()
                    && te.root.getClearType().let { t -> ALLOWED_ROOM_TYPES.any { it == t } }
                    && !te.isEdition()
        }

        var instance: RemoteImService? = null

        private const val notifyId = 7

        private val mCoroutineScope: CoroutineScope by lazy {
            CoroutineScope(Dispatchers.IO)
        }

        fun downloadMedia(session: Session?, mc: MessageWithAttachmentContent) {
            if (session?.fileService()?.isFileInCache(mc) == false)
            {
                val fs = session.fileService()

                if (fs.fileState(mc) != FileService.FileState.Downloading)
                {
                    mCoroutineScope.launch {
                        val url = mc.getFileUrl()

                        if (url?.startsWith("mxc") == true)
                        {
                            try {
                                fs.downloadFile(mc.getFileName(), mc.mimeType, url,
                                    mc.encryptedFileInfo?.toElementToDecrypt())
                            }
                            catch (throwable: Throwable) {
                                throwable.printStackTrace()
                            }
                        }
                    }
                }
            }
        }
    }

    private val mForegroundNotification by lazy {
        NotificationCompat.Builder(this, KeanuConstants.NOTIFICATION_CHANNEL_ID_SERVICE)
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.drawable.notify_app)
                .setOngoing(true)
                .setAutoCancel(false)
                .setWhen(System.currentTimeMillis())
                .setContentText(getString(R.string.app_unlocked))
                .setContentIntent(PendingIntent.getActivity(this, 0,
                        mStatusBarNotifier?.getDefaultIntent(), PendingIntent.FLAG_UPDATE_CURRENT))
                .build()
    }


    override fun onCreate() {
        Timber.d("ImService started")

        mStatusBarNotifier = StatusBarNotifier(this)

        initService()
        initNotificationListener()

    }

    private fun initService() {
        instance = this

        val prev = Debug.getTrail(this, SERVICE_CREATE_TRAIL_KEY)
        if (prev != null) Debug.recordTrail(this, PREV_SERVICE_CREATE_TRAIL_TAG, prev)
        Debug.recordTrail(this, SERVICE_CREATE_TRAIL_KEY, Date())

        val prevConnections = Debug.getTrail(this, CONNECTIONS_TRAIL_TAG)
        if (prevConnections != null) Debug.recordTrail(this, PREV_CONNECTIONS_TRAIL_TAG, prevConnections)
        Debug.recordTrail(this, CONNECTIONS_TRAIL_TAG, "0")

        Debug.onServiceStart()


    }

    private fun initNotificationListener() {

        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN, Membership.INVITE)
        val queryParams = builder.build()

        mSession?.getRoomSummariesLive(queryParams)?.observeForever { roomSums : List<RoomSummary> ->

            for (roomSum in roomSums) {

                if (roomSum.hasNewMessages)
                {
                    //if a room has new messages, we should get the timeline
                    getTimeline(mSession?.getRoom(roomSum.roomId))
                }

            }
        }
    }

    fun getTimeline(room: Room?): Timeline? {
        if (room == null) return null

        return if (!mTimelines.containsKey(room.roomId)) {
            val ts = TimelineSettings(50, true)
            val timeline = room.createTimeline(null, ts)

            timeline.addListener(object : Timeline.Listener {
                override fun onTimelineUpdated(snapshot: List<TimelineEvent>) {}

                override fun onTimelineFailure(throwable: Throwable) {}

                override fun onNewTimelineEvents(eventIds: List<String>) {
                    for (tEventId in eventIds) {
                        room.getTimeLineEvent(tEventId)?.let {
                            if (filterEvents(it)) handleTimelineEventNotification(it)
                        }
                    }
                }
            })

            timeline.start()
            mTimelines[room.roomId] = timeline
            mRoomNotifyMap[room.roomId] = true

            val roomNotify = room.getLiveRoomNotificationState()

            roomNotify.observeForever { roomNotificationState: RoomNotificationState ->
                val doNotify = roomNotificationState == RoomNotificationState.ALL_MESSAGES
                mRoomNotifyMap[room.roomId] = doNotify
            }

            timeline
        }
        else {
            mTimelines[room.roomId]
        }
    }

    private fun handleTimelineEventNotification(te: TimelineEvent) {
        // Ignore, when our user sent this.
        if (mSession?.myUserId == te.senderInfo.userId) return

        // Ignore, when notifications for room are disabled.
        if (mRoomNotifyMap.containsKey(te.roomId) && mRoomNotifyMap[te.roomId] == false) return

        val mc = te.getLastMessageContent()
        if (mc is MessageWithAttachmentContent) downloadMedia(mSession, mc)

        mStatusBarNotifier?.notifyChat(te)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (mIsFirstTime) {
            startForeground(notifyId, mForegroundNotification)
            mIsFirstTime = false
        }

        if (ImServiceConstants.EXTRA_CHECK_SHUTDOWN == intent?.action) {
            stopSelf()
        }

        initNotificationListener()

        return START_STICKY
    }

    override fun onLowMemory() {
        Timber.d("onLowMemory()!")
    }

    /**
     * Release memory when the UI becomes hidden or when system resources become low.
     * @param level the memory-related event that was raised.
     */
    override fun onTrimMemory(level: Int) {
        Timber.d("OnTrimMemory: $level")
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    fun cancelNotification(id: String) {
        mStatusBarNotifier?.cancelNotification(id)
    }
}
