package info.guardianproject.keanu.core.util

import android.content.Context
import kotlin.math.ceil

object DisplayHelper {

    private var density: Double? = null

    fun pix2dp(pixel: Int, context: Context): Int {
        if (pixel == 0) return 0

        if (density == null) {
            density = try {
                context.resources.displayMetrics.density.toDouble()
            }
            catch (e: Exception) {
                1.0
            }
        }

        return ceil(density!! * pixel.toDouble()).toInt()
    }
}