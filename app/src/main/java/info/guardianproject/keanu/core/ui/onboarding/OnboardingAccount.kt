package info.guardianproject.keanu.core.ui.onboarding

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by n8fr8 on 6/4/15.
 */
@Parcelize

class OnboardingAccount constructor(

        val username: String? = null,

        val server: String? = null,

        val password: String? = null,

        val isNew: Boolean = false
) : Parcelable